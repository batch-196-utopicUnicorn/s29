



//Query Operators
//Allows us to expand our queryies and define conditions
//instead of just looking specific values

//$gt, $lt, $gte, $lte

//$gt - query operator which means greater than
db.products.find({price:{$gt:3000}})

//$lt - query operator which means less than
db.products.find({price:{$lt: 3000}})


//$gte - query operator which means greater than or equal to
db.products.find({price:{$gte: 30000}})

//$lte - query operator which means less than or equal To
db.products.find({price:{$lte: 2800}})


//$regex - query operator which will allow us to find documents which will match the characters/pattern  of the characters we are looking for.

//$regex - looks for documents with partial match and by default is case sensitive
db.users.find({firstname:{$regex: "O"}})

//$options - used our regenx will be case insensitive
db.users.find({firstname:{$regex: "O", $options: "$i"}})

//You can also find for documents with partial matches
db.products.find({name:{$regex: "phone", $options: "$i"}})

//find users whose email have the word "web" in it
db.users.find({email:{$regex: "web", $options: "i"}})

db.products.find({name:{$regex: "rakk", $options: "$i"}})
db.products.find({name:{$regex: "razer", $options: "$i"}})

//$or $and - logical operators - works almost the same way as they in JS 
//$or - allows us to have a logical operation to find for documents which can satisfy at least one of our conditions

//db.products.find({$or:[{name:{$regex: 'x', $options: "$i"}}, {price:{$lte:10000}}]})
//db.products.find({$or:[{name:{$regex: 'x', $options: '$i'}}, {price:{$gte: 30000}}]})

//$and - look or find documents that satisfies both conditions

db.products.find({$and:[{name:{$regex: 'razer', $options: '$i' }}, {price:{$gte:3000}}]})
db.products.find({$and:[{name:{$regex: 'x', $options: '$i' }}, {price:{$gte:30000}}]})

db.users.find({$and:[{lastname:{$regex: "w", $options: '$i'}},{isAdmin: false}]})
db.users.find({$and:[{firstname:{$regex: "a", $options: '$i'}},{isAdmin: true}]})
db.users.find({})

//Field Projection - allows us to show/hide certain properties/fields
//db.collection.find({queries},{projection}) - 0 means hide, 1 means show

db.users.find({},{_id:0, password:0})
db.users.find({isAdmin: true},{_id:0, email:1})
//_id field must be explicitly hidden.
//We can also just pick which fields to show.
db.users.find({isAdmin: true},{firstname:1, lastname:1})
db.products.find({price:{$gte: 10000,}}, {_id:0, name: 1, price:1})